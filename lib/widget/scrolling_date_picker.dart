import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_custom_widget/widget/wheel_scroll_vew_widget.dart';

const idMonths = [
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
];

const minYear = 1900;

class ScrollingDatePicker extends StatefulWidget {
  final DateTime initialDate;
  const ScrollingDatePicker({this.initialDate, Key key}) : super(key: key);

  static Future<DateTime> show({DateTime initialDate}) {
    return Get.bottomSheet(
      ScrollingDatePicker(initialDate: initialDate),
      enableDrag: false,
    );
  }

  @override
  _ScrollingDatePickerState createState() => _ScrollingDatePickerState();
}

class _ScrollingDatePickerState extends State<ScrollingDatePicker> {
  int _selectedDay = 1;
  int _selectedMonth = 0;
  int _selectedYear = 0;

  @override
  void initState() {
    super.initState();
    final _initialDate = widget.initialDate;
    if (_initialDate != null) {
      _selectedDay = _initialDate.day;
      _selectedMonth = _initialDate.month - 1;
      _selectedYear = _initialDate.year - minYear;
    } else {
      final today = DateTime.now();
      _selectedDay = today.day;
      _selectedMonth = today.month - 1;
      _selectedYear = today.year - minYear;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16),
          topRight: Radius.circular(16),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 16),
            child: Text(
              'Date of Birth',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Flexible(
            child: SizedBox(
              height: 150,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Center(
                    child: Container(
                      width: Get.width,
                      height: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.teal,
                      ),
                    ),
                  ),
                  Container(
                    color: Colors.transparent,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Flexible(
                          flex: 1,
                          child: WheelScrollViewWidget(
                            items: List.generate(
                                31, (index) => (index + 1).toString()),
                            initialIndex: _selectedDay - 1,
                            onChanged: (index) {
                              _selectedDay = index + 1;
                            },
                          ),
                        ),
                        Flexible(
                          flex: 2,
                          child: WheelScrollViewWidget(
                            items: idMonths,
                            initialIndex: _selectedMonth,
                            onChanged: (index) {
                              _selectedMonth = index;
                            },
                          ),
                        ),
                        Flexible(
                          flex: 1,
                          child: WheelScrollViewWidget(
                            items: List.generate(
                                DateTime.now().year - minYear + 1,
                                (index) => (minYear + index).toString()),
                            initialIndex: _selectedYear,
                            onChanged: (index) {
                              _selectedYear = index;
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          ElevatedButton(
              child: Text('CHOOSE'),
              onPressed: () {
                final _chosenDate = DateTime(
                    _selectedYear + minYear, _selectedMonth + 1, _selectedDay);
                Get.back(result: _chosenDate);
              }),
        ],
      ),
    );
  }
}
