import 'package:flutter/material.dart';

class WheelScrollViewWidget extends StatefulWidget {
  final void Function(int) onChanged;
  final int initialIndex;
  final List<String> items;
  const WheelScrollViewWidget({
    this.items,
    this.initialIndex,
    this.onChanged,
    Key key,
  }) : super(key: key);

  @override
  _WheelScrollViewWidgetState createState() => _WheelScrollViewWidgetState();
}

class _WheelScrollViewWidgetState extends State<WheelScrollViewWidget> {
  int _selectedIndex = 0;
  PageController _controller;

  @override
  void initState() {
    super.initState();
    _selectedIndex = widget.initialIndex;
    _controller =
        PageController(viewportFraction: 0.3, initialPage: widget.initialIndex);
  }

  @override
  Widget build(BuildContext context) {
    return PageView.builder(
      physics: const BouncingScrollPhysics(),
      scrollDirection: Axis.vertical,
      controller: _controller,
      onPageChanged: (int index) {
        widget.onChanged(index);
        setState(() {
          _selectedIndex = index;
        });
      },
      itemCount: widget.items.length,
      itemBuilder: (context, position) {
        final item = widget.items[position];
        final scaleFactor = (position == _selectedIndex ? 1 : 0.7).toDouble();
        return Center(
          child: Transform.scale(
            scale: scaleFactor,
            child: Opacity(
              opacity: scaleFactor,
              child: Padding(
                padding: const EdgeInsets.all(2),
                child: Text(
                  item,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Colors.black87,
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
